# language: pt

@pessoa
Funcionalidade: Cadastrar Nova Pessoa 
    Sendo um usuário
    Quero cadastrar uma nova pessoa
    Para que possa validar o cadastro de pessoas

Contexto: Pré condições para acessar Cadastro de Pessoas
    # login
    Dado que eu esteja na tela de Login do i4PRO
    # Quando eu informar no campo usuario "belopes"
    # E eu informar no campo senha "#ndjar8844"
    # E eu clicar no botão Entrar
    # Então deve ser apresentado a Página Inicial 

@novaPessoaFisica
Cenario: Cadastrando uma nova pessoa física
    Dado que acesse menu Cadastros > Pessoas > Cadastro
    E cadaste uma nova pessoa "física"
    E altere as informações da pessoa "física" na aba Detalhes
    E adicione os Papéis de Segurado e Cliente para a pessoa
    E cadastre o endereço
    E adicionar os meios de comunicação
    Quando adicionar a conta corrente
    Então o cadastro da pessoa será concluído com sucesso

@novaPessoaJuridica
Cenario: Cadastrando uma nova pessoa jurídica
    Dado que acesse menu Cadastros > Pessoas > Cadastro
    E cadaste uma nova pessoa "jurídica"
    E altere as informações da pessoa "jurídica" na aba Detalhes
    E adicione os Papéis de Segurado e Cliente para a pessoa
    E cadastre o endereço
    E adicionar os meios de comunicação
    Quando adicionar a conta corrente
    Então o cadastro da pessoa será concluído com sucesso     
