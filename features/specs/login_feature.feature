# language: pt

@login
Funcionalidade: Login do i4PRO
    Sendo um usuario
    Quero fazer login no i4PRO
    Para que eu possa utilizar o sistema

@loginValido
Cenario: Efetuando login com usuário e senha válidos do i4PRO
    Dado que eu esteja na tela de Login do i4PRO
    Quando eu informar no campo usuario "automacao"
    E eu informar no campo senha "Automacao123"
    E eu clicar no botão Entrar
    Então deve ser apresentado a Página Inicial 

@loginInvalido
Esquema do Cenário: Efetuando login com usuário ou senha inválidos do i4PRO
    Dado que eu esteja na tela de Login do i4PRO
    Quando eu informar no campo usuario <usuario>
    E eu informar no campo senha <senha>
    E eu clicar no botão Entrar
    Então para o cenário deve ser apresentado a mensagem <mensagem> 

    Exemplos:
        | cenario   |   usuario     |  senha        | mensagem             | 
        |    1      |    ""         |    ""         |  "Login inválido"    | 
        |    2      |    ""         | "Sancor123"   |  "Login inválido"    | 
        |    3      |   "4340"      |     ""        |  "Login inválido"    | 
        |    4      |   "4340"      | "Sancor1"     |  "Login inválido"    |             
