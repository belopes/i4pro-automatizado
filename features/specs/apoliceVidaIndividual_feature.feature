# language: pt

@apoliceVidaIndividual
Funcionalidade: Emitir uma apólice de vida individual
    Sendo um usuário
    Quero emitir uma apólice de vida individual 
    Para que possa validar a emissão de uma apólice de vida individual

Contexto: Pré condições para acessar Cadastro de Pessoas
    # login
    Dado que eu esteja na tela de Login do i4PRO
    # Quando eu informar no campo usuario "belopes"
    # E eu informar no campo senha "#ndjar8844"
    # E eu clicar no botão Entrar
    # Então deve ser apresentado a Página Inicial

    # 
    Dado que acesse menu Cadastros > Pessoas > Cadastro
    E cadaste uma nova pessoa "física"
    E altere as informações da pessoa "física" na aba Detalhes
    E adicione os Papéis de Segurado e Cliente para a pessoa
    E cadastre o endereço
    E adicionar os meios de comunicação
    Quando adicionar a conta corrente
    Então o cadastro da pessoa será concluído com sucesso

@novaApoliceVidaIndividual
Cenario: Emitindo uma apólice de vida individual
    Dado que inicie a inclusão de uma nova proposta
    E informe os dados do seguro
    E informe os dados do segurado
    E informe os dados do pagamento
    E informe os dados do corretor
    E verifique as regras de aceitação
    E o prêmio seja calculado
    E todas as regras estejam aceitas
    Quando clicar no botão emitir apólice
    Então a apólice de vida individual será criada