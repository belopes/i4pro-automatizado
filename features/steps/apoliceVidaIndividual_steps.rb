menu_page = MenuPage.new
nova_proposta_page = NovaPropostaPage.new

Dado("que inicie a inclusão de uma nova proposta") do
    menu_page.menu_emissao.click
    menu_page.menu_proposta.click
    menu_page.tela_nova_proposta.click
    sleep 1
    
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.campo_emissao_de.select "Liderança"
        nova_proposta.campo_grupo_ramo.select "13\u00a0-\u00a0PESSOAS\u00a0INDIVIDUAL"
        nova_proposta.campo_produto.select "000001 - SANCOR SEGUROS VIDA FÁCIL (PLANO I)"
        nova_proposta.botao_continuar_nova_proposta.click

        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
    end

    puts "Nova Proposta de Vida Individual iniciada"
end

Dado("informe os dados do seguro") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_modulo.select "Módulo\u00a0014\u00a0-\u00a0R$\u00a0200.000,00"
            aba_dados_seguro.botao_continuar_dados_seguro.click
        end
        
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
    end

    puts "Etapa Dados do Seguro concluída"
end
  
Dado("informe os dados do segurado") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_segurado.set $nome
            aba_dados_seguro.botao_pesquisa_segurado.click
        end
    end

    seleciona_primeiro_registro()
    
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_utilizar_endereco.select "A"
            aba_dados_seguro.botao_continuar_dados_segurado.click
        end
        
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
    end

    puts "Etapa Dados do Segurado concluída"
end
  
Dado("informe os dados do pagamento") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_periodicidade_pagamento.select "Mensal"
            aba_dados_seguro.campo_parcelamento.select "1+3"
            aba_dados_seguro.campo_forma_pagto_primeira_parcela.select "756-SICOOB - Boleto Registrado (1000014)"
            aba_dados_seguro.campo_forma_pagto_demais_parcelas.select "756-SICOOB - Boleto Registrado (1000014)"
            aba_dados_seguro.botao_continuar_dados_pagamento.click
        end
        
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
    end

    puts "Etapa Dados do Pagamento concluída"
end
  
Dado("informe os dados do corretor") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_corretor.set "Pessoa 0000050064"
            aba_dados_seguro.botao_pesquisa_corretor.click
        end
    end

    seleciona_primeiro_registro()
    
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.campo_agenciamento.set "20,000"
            aba_dados_seguro.botao_continuar_dados_corretor.click
        end
        
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
    end

    puts "Etapa Dados do Corretor concluída"
end
  
Dado("verifique as regras de aceitação") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.botao_incluir.click
        end
    end

    sleep 1
    nova_proposta_page.botao_OK_tela_confirmacao.click

    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
        puts "Etapa Regras de Aceitação concluída"
        sleep 5
        puts "Nova Proposta Incluída com sucesso!\n Número: #{nova_proposta.label_numero_proposta.text}\n Segurado: #{$nome}"
        take_screenshot("Proposta_Incluida_com_Sucesso", 'passed')
    end
end
  
Dado("o prêmio seja calculado") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.frame_abas_nova_proposta do |aba_dados_seguro|
            aba_dados_seguro.frame_aba_premio do |aba_premio|
                expect(aba_premio.label_valor_total.text).to be_empty
                aba_premio.botao_calcula_premio.click
                sleep 5
                expect(aba_premio.label_mensagem_retorno.text).to have_content("Processamento efetuado com sucesso")
                expect(aba_premio.label_valor_total.text).not_to be_empty
                puts "Prêmio Calculado com sucesso!\n Valor Total do Prêmio: R$#{aba_premio.label_valor_total.text}"
                take_screenshot("Premio_Calculado", 'passed')
            end
        end
    end
end
  
Dado("todas as regras estejam aceitas") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.menu_proposta.click
        nova_proposta.menu_regras_aceitacao.click
    end

    nova_proposta_page.botao_OK_tela_confirmacao.click
    sleep 5

    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        expect(nova_proposta.label_mensagem.text).to have_content("Processamento efetuado com sucesso")

        nova_proposta.frame_abas_nova_proposta do |proposta_abas|
            proposta_abas.aba_regras_aceitacao.click
            puts "Regras aceitas com sucesso"
            sleep 5
            take_screenshot("Regras_de_Aceitação_Aprovadas", 'passed')
            
            # proposta_abas.frame_aba_regras_aceitacao do |aba_regras_aceitacao|
            #     x = 0
            #     while aba_regras_aceitacao.checkbox_aceita.has_xpath?("chkdv_aceita_#{x.to_s}")
            #         puts "passou"
            #         expect(aba_regras_aceitacao.checkbox_aceita.checked).to have_content("checked")
            #         # Adiciona + 1 ao valor de x
            #         x += 1
            #         puts "#{x.to_s}º regra aceita"
            #         aba_regras_aceitacao.checkbox_aceita = 'input[id="chkdv_aceita_'+x.to_s+'"]'
            #     end
            # end
        end
    end
end
  
Quando("clicar no botão emitir apólice") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        nova_proposta.menu_proposta.click
        nova_proposta.menu_gerar_apolice.click
    end

    nova_proposta_page.botao_OK_tela_confirmacao.click
    sleep 5

    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        expect(nova_proposta.label_mensagem.text).to have_content("Apólice emitida com sucesso. Número da apólice:#{nova_proposta.link_apolice.text}")
    end

    puts "Apólice de Vida Individual Emitida com Sucesso"
end
  
Então("a apólice de vida individual será criada") do
    nova_proposta_page.frame_nova_proposta do |nova_proposta|
        numero_apolice = nova_proposta.link_apolice.text.split('/', -1)[0]
        nova_proposta.link_apolice.click
        sleep 1
        expect(nova_proposta.label_numero_apolice.text).to have_content(numero_apolice)
        puts "Apólice número: #{numero_apolice}"
    end
end