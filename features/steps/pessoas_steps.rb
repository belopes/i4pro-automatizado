require 'faker'

menu_page = MenuPage.new
pessoas_page = PessoasPage.new
cpf_cnpj = Faker::CPF.numeric
$nome = ""
tipo_endereco = "Residencial"
numero_aleatorio = rand 1..2000

Dado("que acesse menu Cadastros > Pessoas > Cadastro") do
    menu_page.menu_cadastros.click
    menu_page.menu_pessoas.click
    menu_page.tela_cadastro_pessoa.click
    sleep 1
end

Dado("cadaste uma nova pessoa {string}") do |tipo_pessoa|
    sexo = ""
    if tipo_pessoa == "jurídica"
        $nome = Faker::Company.name
        cpf_cnpj = Faker::CNPJ.numeric
        tipo_endereco = "Comercial"
    elsif numero_aleatorio > 1000
        $nome = Faker::Name.male_first_name + " " + Faker::Name.last_name
        sexo = "Masculino"
    else
        $nome = Faker::Name.female_first_name + " " + Faker::Name.last_name
        sexo = "Feminino"
    end

    pessoas_page.frame_pessoas do |pessoa|
        pessoa.botao_novo.click
        sleep 1
        
        if tipo_pessoa == "física"
            pessoa.campo_tipo_pessoa.select "Física"
            pessoa.campo_nome.set $nome
            pessoa.campo_cpf_cpnj.set cpf_cnpj
            pessoa.campo_sexo.select sexo
        else
            pessoa.campo_tipo_pessoa.select "Jurídica"
            pessoa.campo_nome.set $nome
            pessoa.campo_cpf_cpnj.set cpf_cnpj
        end

        pessoa.botao_inserir.click
        sleep 3
        expect(pessoa.label_nome.text).to eq($nome)
        expect(pessoa.label_cpf_cpnj.text).to eq(cpf_cnpj)
    end

    puts "Pessoa #{tipo_pessoa.capitalize} Criada - Testes Ok\n    Nome: " + $nome + "\n     CPF/CNPJ: " + cpf_cnpj
end
  
Dado("altere as informações da pessoa {string} na aba Detalhes") do |tipo_pessoa|
    pessoas_page.frame_pessoas do |pessoa|
        pessoa.frame_abas_pessoa do |abas_pessoa|
            if tipo_pessoa == "física"
                abas_pessoa.campo_data_nascimento.set "01/02/1993"
                abas_pessoa.campo_profissao.set "AÇOUGUEIRO"
                abas_pessoa.botao_pesquisa_profissao.click
            else
                abas_pessoa.campo_tipo_empresa.select "LTDA"
                abas_pessoa.campo_ramo_atividade.set "AÇOUGUE"
                abas_pessoa.botao_pesquisa_ramo_atividade.click
            end
            sleep 1       
        end
    end

    seleciona_primeiro_registro()

    pessoas_page.frame_pessoas do |pessoa|
        pessoa.frame_abas_pessoa do |abas_pessoa|
            if tipo_pessoa == "física"
                abas_pessoa.botao_alterar_pessoa_fisica.click
            else
                abas_pessoa.botao_alterar_pessoa_juridica.click
            end
            
            sleep 1
            expect(abas_pessoa.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
        end
    end

    puts "Aba Detalhe alterada com sucesso!"
end

Dado("adicione os Papéis de Segurado e Cliente para a pessoa") do
    papeis = ["Segurado", "Cliente"]
    pessoas_page.frame_pessoas do |pessoa|
        pessoa.aba_papeis.click

        pessoa.frame_abas_pessoa do |abas_pessoa|
            abas_pessoa.botao_novo_papel.click
            
            papeis.each do |papel|
                abas_pessoa.campo_papel.select papel
                abas_pessoa.botao_inserir.click
                expect(abas_pessoa.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
                puts "Papel de #{papel} adicionado com sucesso!"
                if papel == "Segurado"
                    abas_pessoa.menu_hamburguer.click
                    abas_pessoa.menu_novo_papel.click
                end
            end
        end
    end   
end

Dado("cadastre o endereço") do
    pessoas_page.frame_pessoas do |pessoa|
        pessoa.aba_enderecos.click

        pessoa.frame_abas_pessoa do |abas_pessoa|
            abas_pessoa.botao_novo_endereco.click
            abas_pessoa.campo_cep.set "87013230"
            abas_pessoa.botao_pesquisa_cep.click
            sleep 1       
        end
    end

    seleciona_primeiro_registro()

    pessoas_page.frame_pessoas do |pessoa|
        pessoa.frame_abas_pessoa do |abas_pessoa|
            abas_pessoa.campo_numero_casa.set numero_aleatorio.to_s
            abas_pessoa.campo_tipo_endereco.select tipo_endereco
            abas_pessoa.checkbox_endereco_padrao.click
            abas_pessoa.botao_inserir.click
            sleep 1
            expect(abas_pessoa.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
        end
    end

    puts "Endereço adicionado com sucesso!"
end

Dado("adicionar os meios de comunicação") do
    # Adicionado o caractere espaço utilizando o código '\u00a0' em 'Telefone'.
    # A opção não é localizada caso seja utilizada a tecla 'barra de espaço' para adiconar o caractere espaço.
    # Devido ao html estar apresentado o código '&nbsp;' para o caractere espaço.
    formas_contato = ["E-mail", "Celular", "Telefone"+"\u00a0"+tipo_endereco.downcase]
    meios_contato = ["belopes@sancorseguros.com", "998541256", "3002-5412"]
    x = 0
    
    while x <= 2
        pessoas_page.frame_pessoas do |pessoa|
            if x == 0
                pessoa.aba_meios_comunicacao.click
            end
            
            pessoa.frame_abas_pessoa do |abas_pessoa|
                abas_pessoa.botao_novo_meio_comunicacao.click
                abas_pessoa.campo_meio_comunicacao.select formas_contato[x]
                if x != 2
                    abas_pessoa.campo_subtipo.select "Comunicação"
                end
                
                if x != 0
                    abas_pessoa.campo_ddd.set "44"
                end
                
                abas_pessoa.campo_meios_contato.set meios_contato[x]
                abas_pessoa.campo_contato.set $nome
                abas_pessoa.checkbox_principal.click
                abas_pessoa.botao_inserir.click
                sleep 1
                expect(abas_pessoa.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
            end
        end
    
        puts formas_contato[x] + " adicionado com sucesso!"
        # Adiciona + 1 ao valor de x
        x += 1
    end
end

Quando("adicionar a conta corrente") do
    pessoas_page.frame_pessoas do |pessoa|
        pessoa.aba_conta_corrente.click

        pessoa.frame_abas_pessoa do |abas_pessoa|
            abas_pessoa.botao_novo_conta_corrente.click
            abas_pessoa.campo_banco.select "BANCO\u00a0DO\u00a0BRASIL\u00a0S.A.\u00a0(Cód.:\u00a0001)"
            abas_pessoa.campo_agencia.set "2709"
            abas_pessoa.campo_digito_agencia.set "0"
            abas_pessoa.campo_conta.set "77591"
            abas_pessoa.campo_digito_conta.set "6"
            abas_pessoa.campo_tipo_conta_corrente.select "Própria"
            abas_pessoa.campo_tipo_conta_bancaria.select "01\u00a0-\u00a0Conta\u00a0corrente\u00a0individual"
            abas_pessoa.checkbox_conta_principal.click
            abas_pessoa.checkbox_conta_ativa.click
            abas_pessoa.checkbox_conta_devolucao.click
            abas_pessoa.botao_inserir.click
            expect(abas_pessoa.label_mensagem.text).to have_content("Processamento efetuado com sucesso")
        end
    end

    puts "Conta Corrente adicionada com sucesso!"   
end
  
Então("o cadastro da pessoa será concluído com sucesso") do

end