login_page = LoginPage.new

Dado("que eu esteja na tela de Login do i4PRO") do
  login_page.load
end

Quando("eu informar no campo usuario {string}") do |usuario|
  #unless expect(login_page.label_ERP.text).to have_content("ERP")
 
    login_page.campo_usuario.set usuario
  #end
end

Quando("eu informar no campo senha {string}") do |senha|
  #unless expect(login_page.label_ERP.text).to have_content("ERP")
    login_page.campo_senha.set senha
  #end
end

Quando("eu clicar no botão Entrar") do
  #unless expect(login_page.label_ERP.text).to have_content("ERP")
    login_page.botao_entrar.click
  #end
end

Então("deve ser apresentado a Página Inicial") do
  sleep 3
  expect(login_page.label_ERP.text).to have_content("ERP")
end

Então("para o cenário deve ser apresentado a mensagem {string}") do |mensagem|
  sleep 3
  expect(login_page.msg_login_invalido.text).to have_content(mensagem)
end

