require 'site_prism'

class MenuPage < SitePrism::Page
    set_url ""
    
    #acessando tela de pesquisa de pessoas
    element :menu_cadastros, 'a[id="linkMenu_100"]'
    element :menu_pessoas, 'a[id="menu_600218"]'
    element :tela_cadastro_pessoa, 'a[id="subMenu600219"]'

    #acessando tela de nova proposta
    element :menu_emissao, 'a[id="linkMenu_700"]'
    element :menu_proposta, 'a[id="menu_702"]'
    element :tela_nova_proposta, 'a[id="subMenu1000011"]'
end
