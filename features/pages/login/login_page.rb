require 'site_prism'

class LoginPage < SitePrism::Page
    set_url ""
    
    element :campo_usuario, 'input[name="cd_usuario"]'
    element :campo_senha, 'input[name="nm_senha"]'
    element :botao_entrar, 'input[id="botaoEntrar"]' 
    element :label_ERP, 'label[id="labelAbaModulo"]' 
    element :msg_login_invalido, 'div[id="containermsgErro"]' 
end