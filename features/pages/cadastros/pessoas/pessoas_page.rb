require 'site_prism'

class PessoasPage < SitePrism::Page   
    
    class PessoasFrame < SitePrism::Page
        # tela de pesquisa de pessoas
        element :botao_novo, 'button[id="TRBTNC_13675"]'

        # tela de cadastro de pessoas
        element :campo_tipo_pessoa, 'select[id="cd_tipo_pessoa_novo"]'
        element :campo_nome, 'input[id="nm_pessoa"]'
        element :campo_cpf_cpnj, 'input[id="nr_cnpj_cpf"]'
        element :campo_sexo, 'select[id="id_sexo"]'
        element :botao_inserir, 'button[id="TRBTNC_a999999"]'
        
        # pessoa já cadastrada
        element :label_nome, 'label[id="txtnm_pessoa"]'
        element :label_cpf_cpnj, 'label[id="txtnr_cnpj_cpf"]'
        
        # aba detalhe
        element :aba_detalhe_pessoa_fisica, 'a[id="BTN_13515"]'
        element :aba_detalhe_pessoa_juridica, 'a[id="BTN_13526"]'
        element :aba_papeis, 'a[id="BTN_13531"]'
        element :aba_enderecos, 'a[id="BTN_13527"]'
        element :aba_meios_comunicacao, 'a[id="BTN_13528"]'
        element :aba_conta_corrente, 'a[id="BTN_13529"]'
        
        
        class AbasPessoaFrame < SitePrism::Page
            # elementos presentes em todas as abas
            element :label_mensagem, 'label[id="lblMensagem"]'
            element :botao_inserir, 'button[id="TRBTNC_a999999"]'

            # elementos da aba detalhe
            element :campo_data_nascimento, 'input[id="dt_nascimento"]'
            element :campo_profissao, 'input[id="txtcd_profissao"]'
            element :botao_pesquisa_profissao, 'button[id="lupa36084"]'
            element :botao_alterar_pessoa_fisica, 'button[id="TRBTNC_13538"]'
            
            element :campo_tipo_empresa, 'select[id="cd_tp_empresa"]'
            element :campo_ramo_atividade, 'input[id="txtid_ramo_atividade"]'
            element :botao_pesquisa_ramo_atividade, 'button[id="lupa36114"]'
            element :botao_alterar_pessoa_juridica, 'button[id="TRBTNC_13603"]'

            # elementos da aba papéis
            element :botao_novo_papel, 'button[id="TRBTNC_13631"]'
            element :campo_papel, 'select[id="cd_papel"]'

            # elementos da aba endereços
            element :botao_novo_endereco, 'button[id="TRBTNC_13634"]'
            element :campo_cep, 'input[id="txtnm_cep"]'
            element :botao_pesquisa_cep, 'button[id="lupa36704"]'
            element :campo_numero_casa, 'input[id="nr_rua_endereco"]'
            element :campo_tipo_endereco, 'select[id="id_tp_endereco"]'
            element :checkbox_endereco_padrao, 'input[id="dv_endereco_padrao"]'

            # elementos da aba meios de comunicação
            element :botao_novo_meio_comunicacao, 'button[id="TRBTNC_13584"]'
            element :campo_meio_comunicacao, 'select[id="cd_tp_meio_comunicacao"]'
            element :campo_subtipo, 'select[id="cd_subtipo_meio_comunicacao"]'
            element :campo_ddd, 'input[id="nr_ddd"]'
            element :campo_meios_contato, 'input[id="nm_meio_comunicacao"]'
            element :campo_contato, 'input[id="nm_contato"]'
            element :checkbox_principal, 'input[id="dv_principal"]'

            # elementos da aba contas correntes
            element :botao_novo_conta_corrente, 'button[id="TRBTNC_13594"]'
            element :campo_banco, 'select[id="nr_banco"]'
            element :campo_agencia, 'input[id="nr_agencia"]'
            element :campo_digito_agencia, 'input[id="nr_dac_agencia"]'
            element :campo_conta, 'input[id="nr_conta_corrente"]'
            element :campo_digito_conta, 'input[id="nr_dac_conta_corrente"]'
            element :campo_tipo_conta_corrente, 'select[id="cd_tp_identificacao_conta_corrente"]'
            element :campo_tipo_conta_bancaria, 'select[id="id_tp_conta_banco"]'
            element :checkbox_conta_principal, 'input[id="dv_principal"]'
            element :checkbox_conta_ativa, 'input[id="dv_ativa"]'
            element :checkbox_conta_devolucao, 'input[id="dv_conta_restituicao"]'
            element :menu_hamburguer, 'button[id="buttonMenuSuspenso"]'
            element :menu_novo_papel, 'a[id="menu13535"]'
            
        end

        iframe :frame_abas_pessoa, AbasPessoaFrame, 'iframe[id="IFRM"]'
    end

    iframe :frame_pessoas, PessoasFrame, 'iframe[id="ifrmPai"]'
end
