require 'site_prism'

class PesquisaPage < SitePrism::Page   
    
    class JanelaPesquisaFrame < SitePrism::Page
        element :primeiro_registro_pesquisa, 'td[id="TDLINK_1"]'
    end

    iframe :frame_janela_pesquisa, JanelaPesquisaFrame, 'iframe[id="ifrmJanela"]'
end