require 'site_prism'

class NovaPropostaPage < SitePrism::Page   
    
    # classe referente ao frame de etapas de inclusão da proposta
    class NovaPropostaFrame < SitePrism::Page
        # tela Nova Proposta
        element :campo_emissao_de, 'select[id="cd_tp_origem"]'
        element :campo_grupo_ramo, 'select[id="id_grp_ramo"]'
        element :campo_produto, 'select[id="cd_produto"]'
        element :botao_continuar_nova_proposta, 'button[id="TRBTNC_1000195"]'
        
        # mensagem
        element :label_mensagem, 'label[id="lblMensagem"]'
        element :link_apolice, 'a[target="_self"]'

        # menu de ações proposta
        element :menu_proposta, 'button[id="buttonMenuSuspenso"]'
        element :menu_regras_aceitacao, 'a[id="menu12581"]' #'a[id="menu1003154"]'
        element :menu_gerar_apolice, 'a[id="menu1710"]' #'a[id="menu1003128"]'

        #tela apólice
        element :label_numero_apolice, 'label[id="txtcd_apolice"]'

        # classe referente ao frame de abas das etapas de inclusão da proposta
        class AbasNovaProposta < SitePrism::Page
            # aba dados do seguro
            element :campo_modulo, 'select[id="id_faixa_cotacao"]'
            element :botao_continuar_dados_seguro, 'button[id="TRBTNC_1000210"]'
            
            # aba dados do segurado
            element :campo_segurado, 'input[id="txtid_pessoa_cliente_vi"]' #'input[id="txtid_pessoa_cliente"]'
            element :botao_pesquisa_segurado, 'button[id="lupa1012772"]' #'button[id="lupa1000471"]'
            element :campo_utilizar_endereco, 'select[id="id_endereco"]'
            element :botao_continuar_dados_segurado, 'button[id="TRBTNC_1000216"]'
            
            # aba dados do pagamento
            element :campo_periodicidade_pagamento, 'select[id="id_periodo_pagamento"]'
            element :campo_parcelamento, 'select[id="id_produto_parc_premio"]'
            element :campo_forma_pagto_primeira_parcela, 'select[id="cd_forma_pagamento_pparcela"]'
            element :campo_forma_pagto_demais_parcelas, 'select[id="cd_forma_pagamento"]'
            element :botao_continuar_dados_pagamento, 'button[id="TRBTNC_8000084"]'
            
            # aba dados do corretor
            element :campo_corretor, 'input[id="txtid_pessoa_corretor"]'
            element :campo_agenciamento, 'input[id="pe_agenciamento"]'
            element :botao_pesquisa_corretor, 'button[id="lupa1000438"]'
            element :botao_continuar_dados_corretor, 'button[id="TRBTNC_1001791"]'

            # aba regras de aceitação
            element :botao_incluir, 'button[id="TRBTNC_1001820"]'
            
            # aba prêmio
            class AbaPremio < SitePrism::Page
                element :botao_calcula_premio, 'button[id="TRBTNC_8000111"]'
                element :label_mensagem_retorno, 'label[id="lblnm_retorno_processo"]'
                element :label_valor_total, 'label[id="lblvl_total"]' 
            end
    
            iframe :frame_aba_premio, AbaPremio, 'iframe[eng-idtela="1000056"]'
            
            # aba regras de aceitação por produto
            element :aba_regras_aceitacao, 'a[id="BTN_12580"]'

            class AbaRegrasAceitacao < SitePrism::Page
                element :checkbox_aceita, 'input[id="chkdv_aceita_0"]'
            end
    
            iframe :frame_aba_regras_aceitacao, AbaRegrasAceitacao, 'iframe[eng-idtela="1671"]'
        end

        iframe :frame_abas_nova_proposta, AbasNovaProposta, 'iframe[id="IFRM"]'

        # tela proposta já inclusa
        element :label_numero_proposta, 'label[id="txtcd_proposta"]'
        
    end

    iframe :frame_nova_proposta, NovaPropostaFrame, 'iframe[id="ifrmPai"]'
    
    # modal confimação de inclusão da proposta
    element :botao_OK_tela_confirmacao, 'button[id="btnOkConfirm"]'      
    
    element :botao_calcula_premio, '#TRBTNC_8000111'
end
